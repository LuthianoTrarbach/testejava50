pipeline{
    options
            {
                // buildDiscarder(logRotator(numToKeepStr: '3'))
                disableConcurrentBuilds()
            }
    agent any
    parameters {
        string(name: 'SLACK_CHANNEL_1',
                description: 'Canal no Slack para notificações do Jenkins',
                defaultValue: '#devops')

        string(name: 'VERSION_TO_DEPLOY',
                description: 'Utilizado para realizar o deploy de alguma versão específica',
                defaultValue: '0')
    }
    environment {
        REGISTRY_URL = "570898043353.dkr.ecr.us-east-1.amazonaws.com"
        REGISTRY_HTTPS = "https://570898043353.dkr.ecr.us-east-1.amazonaws.com"
        VERSION = readMavenPom().getVersion()
        IMAGE = readMavenPom().getArtifactId()
        DEPLOYMENT_NAME = readMavenPom().getArtifactId()
        NAMESPACE = "rh"
        ECRCRED = "ecr:us-east-1:aws_credentials"
        CONFIG_NAME = "rh-southsystem"

        // Slack configuration
        SLACK_COLOR_DANGER  = '#E01563'
        SLACK_COLOR_INFO    = '#6ECADC'
        SLACK_COLOR_WARNING = '#FFC300'
        SLACK_COLOR_GOOD    = '#3EB991'
    }
    stages{
        stage("Iniciando"){
            when{
                equals expected: "0", actual: "${params.VERSION_TO_DEPLOY}"
            }
            steps{
                script {
                    committerEmail = sh (
                            script: 'git --no-pager show -s --format=\'%ae\'',
                            returnStdout: true
                    ).trim()
                    env.USER_ID = "${committerEmail}"
                    NEW_VERSION = sh(returnStdout: true, script: """
                        #!/bin/bash
                        echo ${VERSION} | awk -F. -v OFS=. 'NF==1{print ++\$NF}; NF>1{if(length(\$NF+1)>length(\$NF))\$(NF-1)++; \$NF=sprintf("%0*d", length(\$NF), (\$NF+1)%(10^length(\$NF))); print}'
                    """).trim()
                }
                slackSend (color: "${env.SLACK_COLOR_INFO}",
                        channel: "${params.SLACK_CHANNEL_1}",
                        message: "*STARTED:* Job ${env.JOB_NAME} build ${env.BUILD_NUMBER} by ${env.USER_ID}\n More info at: ${env.BUILD_URL}")
                withCredentials([usernamePassword(credentialsId: 'bitbucket_key', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]){
                    sh "git remote set-url origin https://${USERNAME}:${PASSWORD}@bitbucket.org/southsystem/${DEPLOYMENT_NAME}.git"
                    sh "git remote update"
                    sh "git config --add remote.origin.fetch +refs/heads/master:refs/remotes/origin/master"

                    sh """
                        if git ls-remote --exit-code origin refs/tags/${NEW_VERSION} >/dev/null 2>&1
                        then
                            echo "A TAG já existe, por favor execute o comando [git pull origin master] antes de mandar a nova versão."
                            exit 1
                        fi
                    """
                }
            }
        }

        // =====================================================================================
        // ================================= DEVELOP ===========================================
        // =====================================================================================

        stage("Tests Develop"){
            when{
                branch "develop"
                equals expected: "0", actual: "${params.VERSION_TO_DEPLOY}"
            }
            steps{
                sh 'mvn test -s settings.xml'
            }
        }

        stage("Build Develop"){
            when{
                branch "develop"
                equals expected: "0", actual: "${params.VERSION_TO_DEPLOY}"
            }
            steps{
                sh 'mvn clean install -s settings.xml -U -DskipTests=true'
            }
        }

        stage("Sonarqube"){
            when{
                branch "develop"
                equals expected: "0", actual: "${params.VERSION_TO_DEPLOY}"
            }
            steps{
                // withSonarQubeEnv("SonarQube"){
                sh 'mvn clean verify sonar:sonar -s settings.xml -DskipTests'
                // }
            }
        }

        stage("Docker Build Develop"){
            when{
                branch "develop"
                equals expected: "0", actual: "${params.VERSION_TO_DEPLOY}"
            }
            steps{
                sh 'mvn -s settings.xml package -Pprod docker:build'
            }
        }

        stage("Docker Tag Develop"){
            when{
                branch "develop"
                equals expected: "0", actual: "${params.VERSION_TO_DEPLOY}"
            }
            steps{
                sh "docker tag southsystem/${IMAGE} ${REGISTRY_URL}/${IMAGE}:unstable-${BUILD_NUMBER}"
                sh "docker tag southsystem/${IMAGE} ${REGISTRY_URL}/${IMAGE}:unstable"
            }
        }

        stage("Upload Develop to Registry"){
            when{
                branch "develop"
                equals expected: "0", actual: "${params.VERSION_TO_DEPLOY}"
            }
            steps{
                // FUNCIONA!! Com o parâmetro set +x as informações de credenciasi não são mostrados no log 
                withCredentials([[$class: 'AmazonWebServicesCredentialsBinding', credentialsId: 'aws_credentials']]) {
                    sh '''
                        set +x
                        echo \$(aws ecr get-authorization-token --region us-east-1 --output text --query 'authorizationData[].authorizationToken' | base64 -d | cut -d: -f2) | docker login -u AWS ${REGISTRY_HTTPS} --password-stdin
                    '''
                    sh "docker push ${REGISTRY_URL}/${IMAGE}:unstable-${BUILD_NUMBER}"
                    sh "docker push ${REGISTRY_URL}/${IMAGE}:unstable"
                }
            }
        }

        stage("Deploy Specific Version Develop"){
            when{
                branch "develop"
                not {
                    equals expected: "0", actual: "${params.VERSION_TO_DEPLOY}"
                }
            }
            steps{
                wrap([$class: 'org.jenkinsci.plugins.kubernetes.cli.KubectlBuildWrapper', credentialsId: 'kubecfg-test-portal', serverUrl: 'https://api.test.portal.southsystem.com.br']) {
                    sh "kubectl --namespace=${NAMESPACE} set image --record deployment ${DEPLOYMENT_NAME} ${DEPLOYMENT_NAME}=${REGISTRY_URL}/${IMAGE}:unstable-${params.VERSION_TO_DEPLOY}"
                }
            }
        }

        stage("Deploy to Develop"){
            when{
                branch "develop"
                equals expected: "0", actual: "${params.VERSION_TO_DEPLOY}"
            }
            steps{
                wrap([$class: 'org.jenkinsci.plugins.kubernetes.cli.KubectlBuildWrapper', credentialsId: 'kubecfg-test-portal', serverUrl: 'https://api.test.portal.southsystem.com.br']) {
                    sh "kubectl --namespace=${NAMESPACE} set image --record deployment ${DEPLOYMENT_NAME} ${DEPLOYMENT_NAME}=${REGISTRY_URL}/${IMAGE}:unstable-${BUILD_NUMBER}"
                }
            }
            post{
                always{
                    sh "docker rmi ${REGISTRY_URL}/${IMAGE}:unstable-${BUILD_NUMBER}"
                    sh "docker rmi ${REGISTRY_URL}/${IMAGE}:unstable"
                }
            }
        }

        stage("Aprovar"){
            when{
                branch "develop"
                equals expected: "0", actual: "${params.VERSION_TO_DEPLOY}"
            }
            options {
                timeout(time: 5, unit: 'MINUTES')
            }
            steps{
                slackSend (color: "${env.SLACK_COLOR_INFO}",
                        channel: "${params.SLACK_CHANNEL_1}",
                        message: "*WAITING:* Job ${env.JOB_NAME} build ${env.BUILD_NUMBER} by ${env.USER_ID}\n Waiting to Aprove at: ${env.BUILD_URL}")
                input message: 'Enviar para PRODUÇÃO? (Clique em "Proceed" para continuar)'
            }
        }

        stage("Git Merge to Master"){
            when{
                branch "develop"
                equals expected: "0", actual: "${params.VERSION_TO_DEPLOY}"
            }
            steps{
                sh "mvn -s settings.xml versions:set -DnewVersion=${NEW_VERSION} -DskipTests"
                withCredentials([usernamePassword(credentialsId: 'bitbucket_key', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]){
                    sh "git remote set-url origin https://${USERNAME}:${PASSWORD}@bitbucket.org/southsystem/${DEPLOYMENT_NAME}.git"
                    sh "git remote update"
                    sh "git config --add remote.origin.fetch +refs/heads/master:refs/remotes/origin/master"
                    // sh "git fetch origin develop:origin/develop"
                    // sh "git fetch origin master:origin/master"
                    sh "git fetch --no-tags"

                    // sh "git checkout -f ${GIT_COMMIT}"
                    sh "git checkout develop"
                    sh "git add pom.xml"
                    sh "git commit -m 'Update pom.xml version to ${NEW_VERSION}'"

                    sh "git checkout master"
                    sh "git merge -m 'Merge from develop branch by Jenkins Pipeline' develop --no-ff"

                    sh "git tag -a ${NEW_VERSION} -m 'Versão ${NEW_VERSION} gerada'"
                    sh "git push origin ${NEW_VERSION}"

                    sh "git push origin master"
                }
            }
        }

        // =====================================================================================
        // ================================ HOTFIX ===========================================
        // =====================================================================================

        stage("Tests Hotfix"){
            when{
                branch "hotfix"
                equals expected: "0", actual: "${params.VERSION_TO_DEPLOY}"
            }
            steps{
                sh 'mvn test -s settings.xml'
            }
        }

        stage("Aprovar Hotfix"){
            when{
                branch "hotfix"
                equals expected: "0", actual: "${params.VERSION_TO_DEPLOY}"
            }
            options {
                timeout(time: 5, unit: 'MINUTES')
            }
            steps{
                slackSend (color: "${env.SLACK_COLOR_INFO}",
                        channel: "${params.SLACK_CHANNEL_1}",
                        message: "*WAITING:* Job ${env.JOB_NAME} build ${env.BUILD_NUMBER} by ${env.USER_ID}\n Waiting to Aprove at: ${env.BUILD_URL}")
                input message: 'Enviar para PRODUÇÃO? (Clique em "Proceed" para continuar)'
            }
        }

        stage("Git Merge to Master - Hotfix"){
            when{
                branch "hotfix"
                equals expected: "0", actual: "${params.VERSION_TO_DEPLOY}"
            }
            steps{
                sh "mvn -s settings.xml versions:set -DnewVersion=${NEW_VERSION} -DskipTests"
                withCredentials([usernamePassword(credentialsId: 'bitbucket_key', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]){
                    sh "git remote set-url origin https://${USERNAME}:${PASSWORD}@bitbucket.org/southsystem/${DEPLOYMENT_NAME}.git"
                    sh "git remote update"
                    sh "git config --add remote.origin.fetch +refs/heads/master:refs/remotes/origin/master"
                    // sh "git fetch origin develop:origin/develop"
                    // sh "git fetch origin master:origin/master"
                    sh "git fetch --no-tags"

                    // sh "git checkout -f ${GIT_COMMIT}"
                    sh "git checkout hotfix"
                    sh "git add pom.xml"
                    sh "git commit -m 'Update pom.xml version to ${NEW_VERSION}'"

                    sh "git checkout master"
                    sh "git merge -m 'Merge from hotfix branch by Jenkins Pipeline' hotfix --no-ff"

                    sh "git tag -a ${NEW_VERSION} -m 'Versão Hotfix ${NEW_VERSION} gerada'"
                    sh "git push origin ${NEW_VERSION}"

                    sh "git push origin master"
                }
            }
        }

        // =====================================================================================
        // ================================ PRODUÇÃO ===========================================
        // =====================================================================================
        stage("Build/Tests Prod"){
            when{
                branch "master"
                equals expected: "0", actual: "${params.VERSION_TO_DEPLOY}"
            }
            steps{
                sh 'mvn clean install -s settings.xml'
            }
        }

        stage("Docker Build Prod"){
            when{
                branch "master"
                equals expected: "0", actual: "${params.VERSION_TO_DEPLOY}"
            }
            steps{
                sh 'mvn -s settings.xml package -Pprod docker:build'
            }
        }

        stage("Docker Tag Prod"){
            when{
                branch "master"
                equals expected: "0", actual: "${params.VERSION_TO_DEPLOY}"
            }
            steps{
                sh "docker tag southsystem/${IMAGE} ${REGISTRY_URL}/${IMAGE}:${VERSION}"
                sh "docker tag southsystem/${IMAGE} ${REGISTRY_URL}/${IMAGE}:latest"
            }
        }

        stage("Upload to Registry Prod"){
            when{
                branch "master"
                equals expected: "0", actual: "${params.VERSION_TO_DEPLOY}"
            }
            steps{
                // FUNCIONA!! Com o parâmetro set +x as informações de credenciasi não são mostrados no log 
                withCredentials([[$class: 'AmazonWebServicesCredentialsBinding', credentialsId: 'aws_credentials']]) {
                    sh '''
                        set +x
                        echo \$(aws ecr get-authorization-token --region us-east-1 --output text --query 'authorizationData[].authorizationToken' | base64 -d | cut -d: -f2) | docker login -u AWS ${REGISTRY_HTTPS} --password-stdin
                    '''
                    sh "docker push ${REGISTRY_URL}/${IMAGE}:${VERSION}"
                    sh "docker push ${REGISTRY_URL}/${IMAGE}:latest"
                }
            }
        }

        stage("Deploy Specific Version Prod"){
            when{
                branch "master"
                not {
                    equals expected: "0", actual: "${params.VERSION_TO_DEPLOY}"
                }
            }
            steps{
                wrap([$class: 'org.jenkinsci.plugins.kubernetes.cli.KubectlBuildWrapper', credentialsId: 'kubecfg-prod-portal', serverUrl: 'https://api.portal.southsystem.com.br']) {
                    sh "kubectl --namespace=${NAMESPACE} set image --record deployment ${DEPLOYMENT_NAME} ${DEPLOYMENT_NAME}=${REGISTRY_URL}/${IMAGE}:${params.VERSION_TO_DEPLOY}"
                }
            }
        }

        stage("Deploy to Prod"){
            when{
                branch "master"
                equals expected: "0", actual: "${params.VERSION_TO_DEPLOY}"
            }
            steps{
                wrap([$class: 'org.jenkinsci.plugins.kubernetes.cli.KubectlBuildWrapper', credentialsId: 'kubecfg-prod-portal', serverUrl: 'https://api.portal.southsystem.com.br']) {
                    sh "kubectl --namespace=${NAMESPACE} set image --record deployment ${DEPLOYMENT_NAME} ${DEPLOYMENT_NAME}=${REGISTRY_URL}/${IMAGE}:${VERSION}"
                }
            }
            post{
                always{
                    sh "docker rmi ${REGISTRY_URL}/${IMAGE}:${VERSION}"
                    sh "docker rmi ${REGISTRY_URL}/${IMAGE}:latest"
                }
            }
        }
    }
    post {
        always {
            deleteDir()
        }
        success{
            script {
                emailext (
                        subject: "[SUCCESS]: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
                        body: '''${SCRIPT, template="groovy-html.template"}''',
                        to: "devops@southsystem.com.br",
                        attachLog: true
                )
                if(env.committerEmail){
                    emailext (
                            subject: "[SUCCESS]: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
                            body: '''${SCRIPT, template="groovy-html.template"}''',
                            to: "${committerEmail}",
                            attachLog: true
                    )
                }
                slackSend (color: "${env.SLACK_COLOR_GOOD}",
                        channel: "${params.SLACK_CHANNEL_1}",
                        message: "*SUCCESS:* Job ${env.JOB_NAME} build ${env.BUILD_NUMBER} by ${env.USER_ID}\n More info at: ${env.BUILD_URL}")
            }
        }
        failure{
            script {
                emailext (
                        subject: "[FAILURE]: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
                        body: '''${SCRIPT, template="groovy-html.template"}''',
                        to: "devops@southsystem.com.br",
                        attachLog: true
                )
                if(env.committerEmail){
                    emailext (
                            subject: "[FAILURE]: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
                            body: '''${SCRIPT, template="groovy-html.template"}''',
                            to: "${committerEmail}",
                            attachLog: true
                    )
                }
                slackSend (color: "${env.SLACK_COLOR_DANGER}",
                        channel: "${params.SLACK_CHANNEL_1}",
                        message: "*FAILED:* Job ${env.JOB_NAME} build ${env.BUILD_NUMBER} by ${env.USER_ID}\n More info at: ${env.BUILD_URL}")
            }
        }
        unstable{
            script {
                emailext (
                        subject: "[UNSTABLE]: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
                        body: '''${SCRIPT, template="groovy-html.template"}''',
                        to: "devops@southsystem.com.br",
                        attachLog: true
                )
                if(env.committerEmail){
                    emailext (
                            subject: "[UNSTABLE]: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
                            body: '''${SCRIPT, template="groovy-html.template"}''',
                            to: "${committerEmail}",
                            attachLog: true
                    )
                }
                slackSend (color: "${env.SLACK_COLOR_WARNING}",
                        channel: "${params.SLACK_CHANNEL_1}",
                        message: "*UNSTABLE:* Job ${env.JOB_NAME} build ${env.BUILD_NUMBER} by ${env.USER_ID}\n More info at: ${env.BUILD_URL}")
            }
        }
    }
}
