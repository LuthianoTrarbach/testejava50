package ${package};

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import br.com.getnet.credenciamentodigital.lib.builder.PrincipalBuilder;
import br.com.getnet.credenciamentodigital.lib.domain.Principal;
import br.com.getnet.credenciamentodigital.lib.helper.TestHelper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;

import ${package}.core.SampleCore;
import ${package}.App;
import ${package}.domain.Sample;
import ${package}.repository.SampleRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public class SampleResourceTest {

	private final static String invalidId = "*_*";

	@Autowired
	private SampleRepository sampleRepository;

	@Autowired
	private SampleCore sampleCore;

	@Autowired
	private TestRestTemplate restTemplate;

	private Sample buildSample() {
		Sample sample = new Sample();
		sample.setValue(BigDecimal.TEN);
		return sample;
	}

	private void assertObject(Sample origin, Sample target) {
		assertThat(origin.getId()).isNotNull();
		assertThat(origin.getValue()).isEqualTo(target.getValue());
	}

	@Test
	public void create() throws Exception {
		final int databaseSizeBeforeCreate = sampleCore.findAll().size();
		final Sample sample = buildSample();

		final ResponseEntity<Sample> response = restTemplate.postForEntity("/sample", TestHelper.getEntity(sample), Sample.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
		assertThat(sample.getValue()).isEqualTo(response.getBody().getValue());

		final List<Sample> samples = sampleCore.findAll();
		assertThat(samples).hasSize(databaseSizeBeforeCreate + 1);
	}

	@Test
	public void createBadRequest() throws Exception {
		final int databaseSizeBeforeCreate = sampleCore.findAll().size();
		final Sample sample = buildSample();
		sample.setValue(null);

		final ResponseEntity<Object> response = restTemplate.postForEntity("/sample", TestHelper.getEntity(sample), Object.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);

		final List<Sample> samples = sampleCore.findAll();
		assertThat(samples).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	public void update() throws Exception {
		final Sample sample = sampleCore.save(buildSample());
		final int databaseSizeBeforeCreate = sampleCore.findAll().size();
		sample.setValue(BigDecimal.ONE);

		final ResponseEntity<Sample> response = restTemplate.exchange("/sample/{id}", HttpMethod.PUT, TestHelper.getEntity(sample), Sample.class, sample.getId());
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertObject(sample, response.getBody());
		assertThat(sampleCore.findAll()).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	public void updateDifferentIdsBadRequest() throws Exception {
		final Sample sample = sampleCore.save(buildSample());
		final int databaseSizeBeforeCreate = sampleCore.findAll().size();

		final ResponseEntity<Sample> response = restTemplate.exchange("/sample/{id}", HttpMethod.PUT, TestHelper.getEntity(sample), Sample.class, invalidId);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
		assertThat(response.getBody()).isNull();
		final List<Sample> samples = sampleCore.findAll();
		assertThat(samples).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	public void updateBadRequest() throws Exception {
		final Sample sample = sampleCore.save(buildSample());
		final int databaseSizeBeforeCreate = sampleCore.findAll().size();
		sample.setValue(null);

		final ResponseEntity<Sample> response = restTemplate.exchange("/sample/{id}", HttpMethod.PUT, TestHelper.getEntity(sample), Sample.class, sample.getId());
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
		final List<Sample> samples = sampleCore.findAll();
		assertThat(samples).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	public void updateNotFound() throws Exception {
		final Sample sample = sampleCore.save(buildSample());
		sample.setId(invalidId);
		sample.setValue(BigDecimal.ONE);

		final ResponseEntity<Sample> response = restTemplate.exchange("/sample/{id}", HttpMethod.PUT, TestHelper.getEntity(sample), Sample.class, invalidId);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
	}

	@Test
	public void getAll() throws Exception {
		sampleRepository.deleteAll();
		final Sample sample = sampleCore.save(buildSample());

		final ResponseEntity<Sample[]> response = restTemplate.exchange("/sample", HttpMethod.GET, TestHelper.getEntityOnlyHeader(), Sample[].class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

		final Sample[] samples = response.getBody();
		assertThat(samples.length).isEqualTo(1);
		assertObject(sample, samples[0]);
	}

	@Test
	public void getAllEmptyList() throws Exception {

		final ResponseEntity<Sample[]> response = restTemplate.exchange("/sample", HttpMethod.GET, TestHelper.getEntityOnlyHeader(), Sample[].class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

		assertThat(response.getBody()).isNotNull();
		assertThat(response.getBody().length).isEqualTo(0);
	}

	@Test
	public void getOne() throws Exception {
		sampleRepository.deleteAll();
		final Sample sample = sampleCore.save(buildSample());

		final ResponseEntity<Sample> response = restTemplate.exchange("/sample/{id}", HttpMethod.GET, TestHelper.getEntityOnlyHeader(), Sample.class, sample.getId());
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

		assertObject(sample, response.getBody());
	}

	@Test
	public void getOneNotFound() throws Exception {
		sampleRepository.deleteAll();

		final ResponseEntity<Sample> response = restTemplate.exchange("/sample/{id}", HttpMethod.GET, TestHelper.getEntityOnlyHeader(), Sample.class, invalidId);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
	}

	@Test
	public void delete() throws Exception {
		final Sample sample = sampleCore.save(buildSample());

		final ResponseEntity<Void> response = restTemplate.exchange("/sample/{id}", HttpMethod.DELETE, TestHelper.getEntityOnlyHeader(), Void.class,
			sample.getId());
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

		final ResponseEntity<Sample> response2 = restTemplate.exchange("/sample/{id}", HttpMethod.GET, TestHelper.getEntityOnlyHeader(), Sample.class,
			sample.getId());
		assertThat(response2.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
	}

	@Test
	public void deleteNotFound() throws Exception {
		final ResponseEntity<Void> response = restTemplate.exchange("/sample/{id}", HttpMethod.DELETE, TestHelper.getEntityOnlyHeader(), Void.class,
			invalidId);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
	}

	@Before
	public void beforeTest() {
		sampleRepository.deleteAll();
		Principal principal = new PrincipalBuilder().buildAny();
		SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(principal, Collections.emptyList()));
	}
}
