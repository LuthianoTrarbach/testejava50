package ${package}.port.outbound;

public interface PortOutbound {
	
	public void sendMessage(String message);

}
