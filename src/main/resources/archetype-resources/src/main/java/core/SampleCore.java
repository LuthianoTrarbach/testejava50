package ${package}.core;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ${package}.domain.Sample;
import ${package}.port.outbound.PortOutbound;
import ${package}.repository.SampleRepository;
import ${package}.port.inbound.PortInbound;

@Service
@Transactional
public class SampleCore implements PortInbound {
	private static final Logger log = LoggerFactory.getLogger(SampleCore.class);
	private final SampleRepository sampleRepository;
	
	@Autowired
	private PortOutbound portOutbound;

	public SampleCore(SampleRepository sampleRepository) {
		this.sampleRepository = sampleRepository;
	}
	

	public Sample save(Sample sample) {
		final Sample result = sampleRepository.save(sample);
		
		portOutbound.sendMessage("Send message to broker");
		
		return result;
	}

	public List<Sample> findAll() {
        return sampleRepository.findAll();
    }
	
	public Sample findOne(String id){
        return sampleRepository.findOne(id);
    }
    
    public void delete(String id){
        sampleRepository.delete(id);
    }
}
